'use strict';

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.getElementById('root');

function bookValidation (book, validationParams) {
    try {
        for (let param of validationParams) {
            if (!Object.keys(book).includes(param)) {
                console.error('Variable ' + param + ' is absent');
                return false;
            }
        }
        return true;
    } catch (err) {
        console.log(err.message);
        return false;
    }
}

function createUlLiAndAddItToRoot(root, book) {
    let ul = document.createElement('ul');
    let li1 = document.createElement('li');

    li1.textContent = 'author - ' + book.author;
    ul.append(li1);
    let li2 = document.createElement('li');

    li2.textContent = 'name - ' + book.name;
    ul.append(li2);
    let li3 = document.createElement('li');

    li3.textContent = 'price - ' + book.price;
    ul.append(li3);

    root.append(ul);
}

books.forEach((book) => {
    if (bookValidation(book, ['author', 'name', 'price'])) {
        createUlLiAndAddItToRoot(root, book);
    }
});